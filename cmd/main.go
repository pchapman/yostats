package main

import (
	"context"
	"gitlab.com/pchapman/yostats/internal/handlers"
	"gitlab.com/pchapman/yostats/internal/services"
	"log"
)

func main() {
	svc, err := services.NewEnvironmentService()
	if err != nil {
		log.Fatal(err)
	}
	svc.StartUpdates()

	app, err := handlers.NewApp(context.Background(), svc)
	if err != nil {
		log.Fatal(err)
	}
	app.Run()
}
