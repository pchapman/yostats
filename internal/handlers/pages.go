package handlers

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/pchapman/yostats/internal/services"
	"gitlab.com/pchapman/yostats/internal/view"
	"net/http"
)

const (
	homeURI         = "/"
	paramCollection = "collection"
	statusURI       = "/status"
)

type PagesHandler struct {
	envSvc    services.EnvironmentService
	templates *view.Templates
}

func newPagesHandler(envSvc services.EnvironmentService, templates *view.Templates) *PagesHandler {
	return &PagesHandler{
		envSvc:    envSvc,
		templates: templates,
	}
}

func (ph *PagesHandler) registerRoutes(e *echo.Echo) {
	e.GET(homeURI, ph.homeHandler)
	e.GET(statusURI, ph.statusFragmentHandler)
}

func (ph *PagesHandler) homeHandler(c echo.Context) error {
	return ph.templates.RenderHome(c, ph.envSvc.LocationStatus())
}

func (ph *PagesHandler) statusFragmentHandler(c echo.Context) error {
	outputJson := false
	if c.Request().Header.Get("HX-Request") == "" {
		if c.Request().Header.Get("Accept") == "application/json" {
			outputJson = true
		} else {
			// This is not an HTMX AJAX request, redirect to the home page
			return c.Redirect(http.StatusTemporaryRedirect, homeURI)
		}
	}
	if outputJson {
		return c.JSON(http.StatusOK, ph.envSvc.LocationStatus())
	}
	return ph.templates.RenderStatusFragment(c, ph.envSvc.LocationStatus())
}
