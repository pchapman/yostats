package handlers

import (
	"context"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/pchapman/yostats/internal/services"
	"gitlab.com/pchapman/yostats/internal/view"
	"log/slog"
	"net/http"
	"os"
	"strconv"
)

const (
	envListenAddr = "LISTEN_ADDRESS"
	envListenPort = "LISTEN_PORT"
)

type App struct {
	listenAddr string
	port       int
	echo       *echo.Echo
}

func NewApp(ctx context.Context, esvc services.EnvironmentService) (*App, error) {
	addr := os.Getenv(envListenAddr)
	if addr == "" {
		addr = "0.0.0.0"
	}
	portStr := os.Getenv(envListenPort)
	if portStr == "" {
		portStr = "8080"
	}
	port, err := strconv.Atoi(portStr)
	if err != nil {
		return nil, err
	}

	app := &App{
		listenAddr: addr,
		port:       port,
	}

	e := echo.New()

	assetHandler := http.FileServerFS(view.Static())
	e.GET("/static/*", echo.WrapHandler(assetHandler))

	tmpls, err := view.NewTemplates()
	if err != nil {
		e.Logger.Fatalf("failed to create templates: %s", err)
	}

	eh := NewErrorHandler(tmpls)
	e.HTTPErrorHandler = eh.ErrorHandler

	// Set up Middlewares
	e.Use(middleware.Logger())

	// Routes
	ph := newPagesHandler(esvc, tmpls)
	ph.registerRoutes(e)

	app.echo = e
	return app, nil
}

func (app *App) Run() {
	lis := fmt.Sprintf("%s:%d", app.listenAddr, app.port)
	slog.Info("server listening, press ctrl+c to stop", "addr", "http://"+lis)
	if err := http.ListenAndServe(lis, app.echo); err != nil {
		app.echo.Logger.Fatal("server terminated", "error", err)
		os.Exit(1)
	}
}

func (app *App) Stop() {
	err := app.echo.Shutdown(context.Background())
	if err != nil {
		app.echo.Logger.Fatal("error terminating server", "error", err)
	}
}
