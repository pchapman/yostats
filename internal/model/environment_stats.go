package model

import "time"

type EnvironmentStatus struct {
	TemperatureC float32   `json:"temperature_c" yaml:"temperature_c"`
	TemperatureF float32   `json:"temperature_f" yaml:"temperature_f"`
	Updated      time.Time `json:"updated" yaml:"updated"`
}

func (es *EnvironmentStatus) Copy() *EnvironmentStatus {
	return &EnvironmentStatus{
		TemperatureC: es.TemperatureC,
		TemperatureF: es.TemperatureF,
		Updated:      es.Updated,
	}
}

func (es *EnvironmentStatus) SetTempC(tempC float32, when time.Time) {
	es.TemperatureC = tempC
	es.TemperatureF = tempC*9/5 + 32
	es.Updated = when
}

func (es *EnvironmentStatus) SetTempF(tempF float32, when time.Time) {
	es.TemperatureF = tempF
	es.TemperatureC = (tempF - 32) * 5 / 9
	es.Updated = when
}

type LocationStatus struct {
	Name        string             `json:"name" yaml:"name"`
	CurrentTime time.Time          `json:"current_time" yaml:"current_time"`
	Indoors     *EnvironmentStatus `json:"indoors" yaml:"indoors"`
	Outdoors    *EnvironmentStatus `json:"outdoors" yaml:"outdoors"`
	Error       string             `json:"error" yaml:"error"`
	IPs         []string           `json:"ips" yaml:"ips"`
}

func (ls *LocationStatus) Copy() *LocationStatus {
	return &LocationStatus{
		Name:        ls.Name,
		Indoors:     ls.Indoors.Copy(),
		Outdoors:    ls.Outdoors.Copy(),
		CurrentTime: time.Now(),
	}
}
