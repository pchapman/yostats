package services

import (
	"context"
	"fmt"
	"github.com/hectormalot/omgo"
	"gitlab.com/pchapman/weatherflow_go"
	"gitlab.com/pchapman/yostats/internal/model"
	"log"
	"log/slog"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	// envLocationName is a constant that indicates the environment variable for the location name.  If it is not set,
	// then an attempt will be made to obtain the location name from the outdoors weather API.
	envLocationName = "LOCATION_NAME"
	// envLocPos is a constant that indicates the environment variable for the location position.  The value should be
	// in the format of "latitude:longitude".  If it is not set, https://open-meteo.com/ will be not be used for
	// obtaining the outdoor temperature in the location.
	envLocPos      = "LOCATION_POS"
	envW1ThermFile = "W1_THERM_FILE"
	// envWFConfig is a constant that indicates the environment variable for the WeatherFlow configuration.  The value
	// should be in the format of "api_key:station_id".  If it is empty, then the service will not be used for obtaining
	// the outdoor temperature in the location.
	envWFConfig = "WF_CONFIG"
)

type EnvironmentService interface {
	LocationStatus() *model.LocationStatus
	StartUpdates() error
	StopUpdates() error
}

type environmentService struct {
	latitude           float64
	locationStatus     *model.LocationStatus
	longitude          float64
	omgo               *omgo.Client
	quit               chan byte
	w1ThermFile        string
	weatherFlowKey     string
	weatherFlowStation int
	sync.RWMutex
}

func NewEnvironmentService() (EnvironmentService, error) {
	w1ThermFile := os.Getenv(envW1ThermFile)
	if w1ThermFile == "" {
		return nil, fmt.Errorf("%s environment variable required", envW1ThermFile)
	}
	es := &environmentService{
		locationStatus: &model.LocationStatus{
			Name:     os.Getenv(envLocationName),
			Indoors:  &model.EnvironmentStatus{},
			Outdoors: &model.EnvironmentStatus{},
		},
		w1ThermFile: w1ThermFile,
	}
	locstr := os.Getenv(envLocPos)
	if locstr != "" {
		parts := strings.Split(locstr, ":")
		if len(parts) != 2 {
			return nil, fmt.Errorf("%s environment variable must be in the format of lat:lon", envLocPos)
		}
		lat, err := strconv.ParseFloat(parts[0], 32)
		if err != nil {
			return nil, fmt.Errorf("in %s environment variable, latitude must be a float value: %s", envLocPos, err)
		}
		es.latitude = float64(lat)
		lon, err := strconv.ParseFloat(parts[1], 32)
		if err != nil {
			return nil, fmt.Errorf("in %s environment variable, longitude must be a float value: %s", envLocPos, err)
		}
		es.longitude = float64(lon)
		c, err := omgo.NewClient()
		if err != nil {
			return nil, err
		}
		es.omgo = &c
	}
	wfconfig := os.Getenv(envWFConfig)
	if wfconfig != "" {
		parts := strings.Split(wfconfig, ":")
		if len(parts) != 2 {
			return nil, fmt.Errorf("%s environment variable must be in the format of api_key:station_id", envWFConfig)
		}
		es.weatherFlowKey = parts[0]
		if es.weatherFlowKey == "" {
			return nil, fmt.Errorf("in %s environment variable, api_key must not be empty", envWFConfig)
		}
		stationStr := parts[1]
		stationId, err := strconv.ParseInt(stationStr, 10, 32)
		if err != nil {
			return nil, fmt.Errorf("in %s environment variable, station ID must be an integer value: %s", envWFConfig, err)
		}
		es.weatherFlowStation = int(stationId)
	}
	return es, nil
}

func (e *environmentService) ExternalStatus() *model.EnvironmentStatus {
	e.RLock()
	defer e.RUnlock()
	return e.locationStatus.Outdoors.Copy()
}

func (e *environmentService) InternalStatus() *model.EnvironmentStatus {
	e.RLock()
	defer e.RUnlock()
	return e.locationStatus.Indoors.Copy()
}

func (e *environmentService) LocationStatus() *model.LocationStatus {
	addrs, err := net.InterfaceAddrs()
	var ips []string
	ipsErr := ""
	if err != nil {
		ipsErr = "Error Determining IP Addresses"
		log.Printf("Error determining IP addresses: %v", err)
	} else {
		for _, addr := range addrs {
			if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
				if ipnet.IP.To4() != nil {
					ips = append(ips, ipnet.IP.String())
					fmt.Println(ipnet.IP)
				}
			}
		}
		if len(ips) == 0 {
			ipsErr = "No Network Connectivity Found"
		}
	}

	e.RLock()
	defer e.RUnlock()
	c := e.locationStatus.Copy()
	c.Error = ipsErr
	c.IPs = ips
	c.CurrentTime = time.Now()
	return c
}

func (e *environmentService) StartUpdates() error {
	e.Lock()
	defer e.Unlock()
	if e.quit != nil {
		return nil
	}
	e.quit = make(chan byte)
	ticker := time.NewTicker(1 * time.Minute)
	go func() {
		if err := e.executeUpdates(); err != nil {
			slog.Error(err.Error())
		}
		for {
			select {
			case <-ticker.C:
				if err := e.executeUpdates(); err != nil {
					slog.Error(err.Error())
				}
			case <-e.quit:
				ticker.Stop()
				return
			}
		}
	}()
	return nil
}

func (e *environmentService) StopUpdates() error {
	e.Lock()
	defer e.Unlock()
	if e.quit == nil {
		return nil
	}
	e.quit <- 1
	close(e.quit)
	e.quit = nil
	return nil
}

func (e *environmentService) executeUpdates() error {
	e.Lock()
	defer e.Unlock()

	// Read Internal Temperature
	internalTempC, err := parseW1Thermometer(e.w1ThermFile)
	if err != nil {
		return err
	}
	e.locationStatus.Indoors.SetTempC(internalTempC, time.Now())

	// Read External Temperature
	if e.weatherFlowKey != "" {
		obs, err := weatherflow_go.QueryLastestStationObservation(e.weatherFlowKey, e.weatherFlowStation)
		if err != nil {
			return err
		}
		e.locationStatus.Outdoors.SetTempC(obs.List[0].AirTemperature, time.Unix(obs.List[0].Timestamp, 0).Local())
		if e.locationStatus.Name == "" {
			e.locationStatus.Name = obs.StationName
		}
	} else if e.omgo != nil {
		loc, err := omgo.NewLocation(e.latitude, e.longitude)
		if err != nil {
			return err
		}
		cur, err := e.omgo.CurrentWeather(context.TODO(), loc, nil)
		if err != nil {
			return err
		}
		e.locationStatus.Outdoors.SetTempC(float32(cur.Temperature), cur.Time.Time.Local())
	}

	return nil
}
