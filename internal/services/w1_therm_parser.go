package services

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// Reads from the W1 thermal sensor
// https://www.circuitbasics.com/raspberry-pi-ds18b20-temperature-sensor-tutorial/

func parseW1Thermometer(filePath string) (float32, error) {
	// Read data line by line to look for a string that contains t= followed by an integer value
	// If found, parse the string and return the temperature, which is the integer value divided by 1000
	// If not found, return an error

	file, err := os.Open(filePath)
	if err != nil {
		return 0.0, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		idx := strings.Index(line, "t=")
		if idx > -1 {
			temperature, err := strconv.ParseFloat(line[idx+2:], 32)
			if err != nil {
				return 0.0, err
			}

			return float32(temperature) / 1000, nil
		}
	}

	return 0.0, fmt.Errorf("no temperature value found in %s", filePath)
}
