.DEFAULT_GOAL := run

# Location of UPX (https://upx.github.io)
UPX := $(shell command -v upx 2> /dev/null)

VERSION=1.0.0
BUILDFLAGS=-s -w -X 'main.Version=${VERSION}'
PROJECTNAME=yostats
GC=go build -ldflags="${BUILDFLAGS}" -tags=prod

verifydeps:
ifndef UPX
	$(error "UPX is not installed on this system.  https://upx.github.io")
endif

run:
	go run -tags dev .

all: build

build:
	mkdir -p ./build/linux
	$(GC) -o ./build/linux/${PROJECTNAME} cmd/main.go && ${UPX} ./build/linux/${PROJECTNAME}

clean:
	rm -rf ./build
