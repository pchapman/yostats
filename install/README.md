# Installing YoStats

## Hardware and System Setup

The below are taken from the tutorial at https://www.circuitbasics.com/raspberry-pi-ds18b20-temperature-sensor-tutorial/

### Connect the DS18B20 to the Raspberry PI

The DS18B20 has three separate pins for ground, data, and Vcc:

![Pinout Diagram](Raspberry-Pi-DS18B20-Tutorial-DS18B20-Pinout-Diagram.png)

### Wiring for SSH Terminal Output

Follow this wiring diagram to output the temperature to an SSH terminal:

![Wiring Diagram](Raspberry-Pi-DS18B20.png)

R1: 4.7K Ohm or 10K Ohm resistor

### Enable the One-Wire Interface

We’ll need to enable the One-Wire interface before the Pi can receive data from the sensor. Once you’ve connected the DS18B20, power up your Pi and log in, then follow these steps to enable the One-Wire interface:

1. At the command prompt, enter `sudo nano /boot/firmware/config.txt`, then add this to the bottom of the file:

`dtoverlay=w1-gpio`

2. Exit Nano, and reboot the Pi with sudo reboot

3. Log in to the Pi again, and at the command prompt enter `sudo modprobe w1-gpio`

4. Then enter `sudo modprobe w1-therm`

5. Change directories to the `/sys/bus/w1/devices` directory by entering `cd /sys/bus/w1/devices`

6. Now enter ls to list the devices:

![Terminal Device Address](Raspberry-Pi-DS18B20-Temperature-Sensor-Tutorial-One-Wire-Device-Address.png)

In my case, `28-000006637696 w1_bus_master1` is displayed.

7. Now enter `cd 28-XXXXXXXXXXXX` (change the X’s to your own address)

For example, in my case I would enter `cd 28-000006637696`

8. Enter `cat w1_slave` which will show the raw temperature reading output by the sensor:

![Terminal Device Output](Raspberry-Pi-DS18B20-Temperature-Sensor-Tutorial-DS18B20-Raw-Output.png)

Here the temperature reading is `t=28625`, which when divided by 1000 indicates a temperature of 28.625 degrees Celsius.

9. To make the changes permanent, edit `/etc/modules` using `sudo nano /etc/modules`  Ensure the following lines are in the file:

```
w1_gpio
w1_therm
```

## Installing YoStats with SystemD and RSysLog

1. Create system user and group `yostats`:  `sudo useradd yostats -s /sbin/nologin -M`
2. Copy yostats executable into `/usr/local/bin`
3. Copy yostats.service into `/lib/systemd/system`
4. Edit the yostats.service file populating the environment variables appropriately `sudo nano /lib/systemd/system/yostats.service`
5. Ensure permissions for systemd file: `sudo chmod 755 /lib/systemd/system/yostats.service`
6. Enable the service: `sudo systemctl enable yostats.service`
7. Start the service: `sudo systemctl start yostats.service`
8. Verify the service is running by checking logs: `sudo journalctl -f -u yostats`

## Proxy to GateKeeper through NGinx

1. Install NGinx: `sudo apt-get install nginx`
2. Edit config file in `/etc/nginx/sites-available/default`
```
server {
	listen 80 default_server;
	listen [::]:80 default_server;

	root /var/www/html;

	index index.html index.htm index.nginx-debian.html;

	server_name _;

	location / {
	    # Set proxy headers
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            # Proxy to gatekeeper
            proxy_pass http://localhost:8080;
	}
}
```
3. Restart nginx: `sudo systemctl restart nginx`

## Running Raspberry Pi in Kiosk Mode for Display

### In Case of Emergency: Break Glass

I've had mixed results following the following instructions to set up a systemd service to start the chromium browsr in
kiosk mode.  Instead, I recommend that you configure wayfire (the window manager) to autostart chromium as described
here: https://www.raspberrypi.com/tutorials/how-to-use-a-raspberry-pi-in-kiosk-mode/

### Initial Setup

1. Before we get started with this tutorial, we will be first removing some packages that we don’t need for our Raspberry Pi kiosk.

Removing these packages will free up some much-needed memory and reduce the number of packages that will be updated every time you update your Raspberry Pi.

To do this just run the following three commands on your Raspberry Pi. We have split these into three different commands to make them easier to copy and write out.

```
sudo apt purge wolfram-engine scratch scratch2 nuscratch sonic-pi idle3 -y
sudo apt purge smartsim java-common minecraft-pi libreoffice* -y
```

2. Now that we have removed these numerous packages from our Raspberry Pi Kiosk we will need to run some cleanup commands.

To remove any unnecessary lingering packages and to clean out the local repository of retrieved packages run the following two commands on your Raspberry Pi.

```
sudo apt clean
sudo apt autoremove -y
```

3. Now that we have removed the bulk of the unnecessary applications, we must now make sure that our installation of Raspbian is up to date. Also, make sure you have SSH enabled as it is very handy if you need to edit any files later on.

We can use the following two commands within the terminal on our Raspberry Pi to update all the packages to their latest versions.

```
sudo apt update
sudo apt upgrade
```

4. We now need also to install xdotool. This tool will allow our bash script to execute key presses without anyone being on the device. We also install the unclutter package, this will enable us to hide the mouse from the display.

Just run the following command on your Raspberry Pi to install the package.

```
sudo apt install xdotool unclutter sed
```

5. Now that those packages have been installed we can now proceed to the next stage of this tutorial. That is setting up Raspberry Pi OS to auto login to our user. Having to log in every time for a kiosk would be an annoyance.

Desktop autologin is the default behavior but if for some reason you have changed it follow the next few steps to switch it back. Otherwise, skip to step 6 of this tutorial.

    1. Run the following command on your Raspberry Pi to load up the Raspi-config tool. We will be using this tool to enable auto login.

```
sudo raspi-config
```

    2. Now within the tool go to 1 System Options -> S5 Boot / Auto Login -> B4 Desktop Autologin

    3. Desktop autologin should now be enabled and you can safely quit out of the raspi-config tool.

6. Now that we have enabled desktop autologin we need to go ahead and write our kiosk.sh script.

### Writing the Raspberry Pi Kiosk Script

The kiosk script will handle the bulk of the work for our Raspberry Pi Kiosk, including launching Chromium itself and also simulating key presses.

1. Begin writing our kiosk bash script by running the following nano command on the Raspberry Pi.

```
nano ~/kiosk.sh
```

2. Within this file enter the following lines of code, we will explain the important parts of the script so you can better bend it to your needs.

```
#!/bin/bash
```

The very first line defines what the command line interpreter (CLI) should use to try and execute this file.

This is useful for cases where you don’t want to have to specify the specific application required every time you run the script.

```
xset s noblank
xset s off
xset -dpms
```

These three lines are pretty important as they help us stop the Raspberry Pi’s display power management system from kicking in and blanking out the screen.

Basically, these three commands set the current xsession not to blank out the screensaver and then disables the screensaver altogether.

The third line disables the entire “display power management system” meaning that the desktop interface should never blank out the screen.

```
unclutter -idle 0.5 -root &
```

This line runs the program we installed earlier called unclutter.

This application will hide the mouse from the display whenever it has been idle for longer then 0.5 seconds and will remove it even if it is over the root background.

You can adjust the idle timer to the number of seconds you want with each decimal place being a fraction of a second.

If you would prefer to remove the mouse instantly, then remove the `-idle 0.5` from the  command.

```
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' /home/$USER/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' /home/$USER/.config/chromium/Default/Preferences
```

These two lines of the script use sed to search through the Chromium preferences file and clear out any flags that would make the warning bar appear, a behavior you don’t really want happening on your Raspberry Pi Kiosk.

If Chromium ever crashes or is closed suddenly, the lines above will ensure you don’t have to get hold of a mouse and keyboard to clear the warning bar that would typically appear at the top of the browser.

```
/usr/bin/chromium-browser --noerrdialogs --disable-infobars --kiosk http://localhost:8080 &
```

This line launches Chromium with our parameters.

We will go through each of these parameters so you know what you can modify, and how you can modify it.

```
--kiosk
```

This flag sets Chromium to operate in Kiosk mode, this locks it into a particular set of features and only allows limited access to both the web browser and any other OS functionality.

Chromium’s kiosk functionality takes full control of the screen, maximizing Chromium to the full size of your screen and stops user input from being accepted by the OS, effectively trapping the end user within a sandbox.

```
--noerrdialogs
```

This option tells Chromium that it should not display any of its error dialogs to the end user.

It is crucial if you don’t want the end user to know if anything has or is going wrong with Chromium, this goes alongside our code to clear the “exited_cleanly” and “exit_type” state earlier in the code.

```
--disable-infobars
```

We use this to disable Chromium from displaying its info bar to the end user.

The info bar can be used by Chromium to notify them of certain things such as that Chromium is not their default web browser.

Of course, as we are using this as a kiosk, we don’t need the user to know any information that Chromium might want to display.

3. Once you are sure everything is correct, save the file by pressing `CTRL + X` then `Y` and finally `ENTER`.

4. After creating this script we should make sure that our user has execution privileges for it.

You can give the user that owns the script execution privileges by running the following command

```
chmod u+x ~/kiosk.sh
```

### Setting up the Raspberry Pi Kiosk to start at boot

1. Before we get started we need to first utilize the following command to work out what the current display value is.

This value is used for the operating system to know what screen to display the Chromium kiosk to, without it the kiosk will either fail to load or load up on the incorrect screen.

Run the following command to print out the value of the `“$DISPLAY”` system variable. This command must be run directly on your Raspberry Pi and not over SSH.

```
echo $DISPLAY
```

Make sure you remember this value as you may need it in step 3 of this section.

2. To get our Raspberry Pi Kiosk to start at boot we will need to go ahead and create a service file by running the command below.

This service file will tell the operating system what file we want to be executed as well as that we want the GUI to be available before starting up the software.

```
sudo nano /lib/systemd/system/kiosk.service
```

3. Within our kiosk service file, enter the following lines of text.

These lines are what will define our service kiosk service, and that we want to run our kiosk.sh script when the system loads into the operating system.

While entering these lines you may have to modify the `“Environment=DISPLAY=:”` line, replacing the `“0”` with the value that you retrieved from the command you used in step 1 of this section.

Additionally, you will need to make sure you replace `“pi”` with the name of your user. For example, with the username `“pimylifeup“`, the path `“/home/pi/kiosk.sh”` would become `“/home/pimylifeup/kiosk.sh“`.

```
[Unit]
Description=Chromium Kiosk
Wants=graphical.target
After=graphical.target

[Service]
Environment=DISPLAY=:0.0
Environment=XAUTHORITY=/home/pi/.Xauthority
Type=simple
ExecStart=/bin/bash /home/pi/kiosk.sh
Restart=on-abort
User=pi
Group=pi

[Install]
WantedBy=graphical.target
```

Once you have entered everything into the file, save the file by pressing `CTRL + X` followed by `Y` then `ENTER`.

4. Now that we have created the service file for our Raspberry Pi Kiosk we can go ahead and now enable it by running the following command.

By enabling the service, we will allow our Chromium Kiosk to start up at boot automatically and will enable the systemd to service manager to monitor it.

```
sudo systemctl enable kiosk.service
```

5. With the Kiosk service now enabled you can either choose to restart the Raspberry Pi or start the service now by running the following command.

```
sudo systemctl start kiosk.service
```

6. If you ever want to check the status of your Raspberry Pi Kiosk’s service, you can run the command below.

This command will return various information about the service, including previously returned lines from the software which can help you debug what’s going wrong when the service is failed.

```
sudo systemctl status kiosk.service
```

If this command shows the status as “Active: active (running)” then everything is now working as it should be, and your Raspberry Pi Chromium Kiosk should be up and operating correctly.

7. Now with everything up and running correctly, if there is for any reason you want to stop the service from running, you can utilize the following command.

```
sudo systemctl stop kiosk.service
```

By stopping the kiosk service, the service manager will kill off all processes associated with it.

This command will stop our `kiosk.sh` script from running while also terminating the open Chromium browser.

8. Finally, if you ever want to disable your Kiosk, you can utilize the following command.

```
sudo systemctl disable kiosk.service
```

This command will stop the Kiosk service from running on boot until you re-enable it.
