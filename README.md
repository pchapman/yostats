# yostats

## Introduction

A simple application for monitoring temperature via a DS18B20 temperature sensor as well as query for sensor readings
via the [weatherflow API](https://weatherflow.github.io/Tempest/) or [open-meteo API](https://open-meteo.com/).  It is assumed that the DS18B20 temperature sensor
is monitoring indoors temperature and that the tempest weather station is providing outdoor temperature data.

## Installation

To install the app, reference the documentation in the [install directory](/install).

## Displaying Results In Kiosk Mode

If the raspberry PI has a screen and you wish to show the site in kiosk mode, follow the instructions
[here](https://pimylifeup.com/raspberry-pi-kiosk/)
